# MCProject_SP2024

Directed Methods Monte Carlo Project - Spring 2024 @ Kennesaw State University

## Getting started

To use this repository online:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/apapaefs%2Fmcproject_sp2024/HEAD)

## About the Author

[Andreas Papaefstathiou](https://facultyweb.kennesaw.edu/apapaefs/) is Assistant Professor of Physics at Kennesaw State University. This website was created in January 2024 and is updated at best effort basis.
